<div class="well col-sm-8">
    <h4>Organisation created</h4>
    <div>
        <span>Thank you, {{ $organisation->owner->name }}. Your organisation {{ $organisation->name }} was successfully created.</span>
    </div>
</div>
