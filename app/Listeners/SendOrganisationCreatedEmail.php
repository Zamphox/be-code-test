<?php

namespace App\Listeners;

use App\Events\OrganisationCreated;
use Illuminate\Support\Facades\Mail;

class SendOrganisationCreatedEmail
{
    /**
     * Handle the event.
     *
     * @param OrganisationCreated $event
     * @return void
     */
    public function handle(OrganisationCreated $event)
    {
        //
        Mail::to($event->organisation->owner->email)->send(new \App\Mail\OrganisationCreated($event->organisation));
    }
}
