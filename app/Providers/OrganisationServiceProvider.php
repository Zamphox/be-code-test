<?php

namespace App\Providers;

use App\Interfaces\OrganisationServiceInterface;
use App\Services\OrganisationService;
use Illuminate\Support\ServiceProvider;

class OrganisationServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(
            OrganisationServiceInterface::class,
            function () {
                return new OrganisationService();
            }
        );
    }
}
