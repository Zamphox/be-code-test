<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Services\OrganisationService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class OrganisationController
 * @package App\Http\Controllers
 */
class OrganisationController extends ApiController
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        try {
            $request->validate(
                [
                    'name' => 'required|unique:organisations,name|max:255',
                    'owner_user_id' => 'integer|exists:users,id',
                    'subscribed' => 'boolean'
                ]
            );
        } catch (ValidationException $exception) {
            return $this->appendError($exception->validator->errors()->first())->respond();
        }

        return $this->store(new OrganisationService());
    }

    /**
     * @param OrganisationService $service
     *
     * @return JsonResponse
     */
    private function store(OrganisationService $service): JsonResponse
    {
        $organisation = $service->createOrganisation($this->request->all());

        return $this
            ->transformItem('organisation', $organisation, ['user'])
            ->respond();
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function listAll(Request $request): JsonResponse
    {
        $organisationService = new OrganisationService();
        $organisations = $organisationService->getOrganisationsList($request->filter);

        return $this->transformCollection('organisation', $organisations)
            ->respond();
    }
}
