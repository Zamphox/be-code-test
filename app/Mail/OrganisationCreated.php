<?php

namespace App\Mail;

use App\Organisation;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrganisationCreated extends Mailable
{
    use SerializesModels;

    public $subject = 'Organisation created';
    protected $organisation;

    /**
     * OrganisationCreated constructor.
     * @param Organisation $organisation
     */
    public function __construct(Organisation $organisation)
    {
        $this->organisation = $organisation;
    }

    /**
     * @return OrganisationCreated
     */
    public function build()
    {
        return $this->view('mail.organisation_created', ['organisation' => $this->organisation]);
    }
}
