<?php

declare(strict_types=1);

namespace App\Services;

use App\Events\OrganisationCreated;
use App\Organisation;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;

/**
 * Class OrganisationService
 * @package App\Services
 */
class OrganisationService
{
    /**
     * @param array $attributes
     * @return Organisation
     */
    public function createOrganisation(array $attributes): Organisation
    {
        $subscribed = isset($attributes['subscribed']) ? $attributes['subscribed'] : 0;
        $organisation = new Organisation();
        $organisation->name = $attributes['name'];
        $organisation->owner_user_id = isset($attributes['owner_user_id']) ? $attributes['owner_user_id'] : Auth::user()->id;
        $organisation->subscribed = $subscribed;
        $organisation->trial_end = $subscribed ? null : Carbon::now()->addDays(30);

        $organisation->save();
        OrganisationCreated::dispatch($organisation);

        return $organisation;
    }

    /**
     * @param null $filter
     * @return Collection
     */
    public function getOrganisationsList($filter = null): Collection
    {
        $organisationsQuery = Organisation::query();
        $this->filterOrganisationsListQuery($filter, $organisationsQuery);

        $organisations = $organisationsQuery->select(
            ['id', 'name', 'trial_end', 'subscribed', 'created_at']
        )->get();

        return $organisations;
    }

    /**
     * @param $filter
     * @param Builder $organisationsQuery
     * @return Builder
     */
    private function filterOrganisationsListQuery($filter, Builder $organisationsQuery): Builder
    {
        if ($filter) {
            if ($filter === 'subbed') {
                $organisationsQuery->whereSubbed();
            }
            if ($filter === 'trial') {
                $organisationsQuery->whereActiveTrial();
            }
        }

        return $organisationsQuery;
    }
}
