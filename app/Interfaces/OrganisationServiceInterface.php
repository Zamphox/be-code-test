<?php


namespace App\Interfaces;


use App\Organisation;

interface OrganisationServiceInterface
{
    /**
     * @param array $attributes
     * @return Organisation
     */
    public function createOrganisation(array $attributes): Organisation;
}
