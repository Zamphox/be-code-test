<?php

namespace App\Events;

use App\Organisation;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class OrganisationCreated
{
    use Dispatchable;
    use SerializesModels;

    public $organisation;

    /**
     * OrganisationCreated constructor.
     * @param Organisation $organisation
     */
    public function __construct(Organisation $organisation)
    {
        //
        $this->organisation = $organisation;
    }
}
