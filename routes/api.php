<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'AuthController@authenticate');

Route::prefix('organisation')->group(
    function () {
        Route::get('', 'OrganisationController@listAll');
        Route::post('', 'OrganisationController@create')->middleware('auth:api');
    }
);

Route::middleware('auth:api')->group(
    function () {
        Route::get(
            '/user',
            function (Request $request) {
                return $request->user();
            }
        );
    }
);
